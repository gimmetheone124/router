import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:usepower_router/router.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AppState()),
      ],
      child: MaterialApp(
        routes: Routes.route(),
      ),
    );
  }
}

class AppState extends ChangeNotifier{
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Routes {
  static dynamic route(){
    return {
      '/': (BuildContext context) => XScreen(),
    };
  }
  static Route onGenerateRoute(){
    return MaterialPageRoute<bool>(builder: (BuildContext context) => XScreen());
  }
}

class XScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
